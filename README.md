

## Contents

* **Project Titles** 
  - System Dependencies is a simulator for a package management software that uses recursion

* **Overview**
  - a WPF executable that can create a graph and check for dependencies 

* **Getting Started**
    - open with Visual Studio 2017 an run in debug mode
    - issue tracker is here: https://bitbucket.org/Trederburg6694/system-dependencies/issues?status=new&status=open
    - wiki is here: https://bitbucket.org/Trederburg6694/system-dependencies/wiki/Home
    


* **Colophon**
  - Credits -- Todd Rederburg 2019
  - Copyright and License -- Project is released with a MIT Licence because of it's simplicity. You can use my software in any way you want without asking me, but you are not allowed to remove this licence.
  https://bitbucket.org/Trederburg6694/system-dependencies/LICENCE.md